//
//  ReviewCollectionViewCell.swift
//  Spudy
//
//  Created by Shamira Kabir on 11/2/21.
//

import UIKit

class ReviewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
}
